#include <iostream>
#include "SDL.h"
#include <stdio.h>

#ifdef _DEBUG
#include <vld.h>
#endif

#define fps 60
#define window_height 600
#define window_width 800
#define movement 50

using namespace std;

class Sprite{
private:
	SDL_Surface * image;
	SDL_Rect rect;

	int origin_x, origin_y;

public:
	Sprite(Uint32 color, int x, int y, int w = 50, int h = 50) {
		image = SDL_CreateRGBSurface(0, w, h, 32, 0, 0, 0, 0);

		SDL_FillRect(image, NULL, color);

		rect = image->clip_rect;

		origin_x = rect.w/2;
		origin_y = rect.h/2;

		rect.x = x - origin_x;
		rect.y = y - origin_y;
	}
	void update() {
	}
	void draw(SDL_Surface* destination) {
		SDL_BlitSurface(image, NULL, destination, &rect);
	}
	void move(int x, int y) {
		rect.x += x;
		rect.y += y;
	}
};


int main(int argc, char* argv[]) {

	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_Window *window = NULL;
	window = SDL_CreateWindow(	"Window.exe",
								SDL_WINDOWPOS_UNDEFINED,
								SDL_WINDOWPOS_UNDEFINED,
								800,
								600,
								SDL_WINDOW_SHOWN);

	if (window == NULL) {
		cout << "Error: " << endl << SDL_GetError() << endl;
	}
	
	SDL_Surface* screen = SDL_GetWindowSurface(window);

	Uint32 color = SDL_MapRGB(screen->format, 100, 100, 150);
	Uint32 colorSprite = SDL_MapRGB(screen->format, 0, 0, 150);

	SDL_FillRect(screen, NULL, color);

	Sprite* player = new Sprite(colorSprite, window_width / 2, window_height / 2);

	Uint32 primer_tick;

	SDL_Event event;
	bool running = true;

	while (running) {

		primer_tick = SDL_GetTicks();

		SDL_FillRect(screen, NULL, color);

		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_QUIT:
				running = false;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym) {
				case SDLK_ESCAPE:
					running = false;
					break;
				
				case SDLK_RIGHT:
					player->move(movement, 0);
					break;
				case SDLK_LEFT:
					player->move(-movement, 0);
					break;
				case SDLK_UP:
					player->move(0, -movement);
					break;
				case SDLK_DOWN:
					player->move(0, movement);
					break;

				}
			}
		}

		if ((1000 / fps) > SDL_GetTicks() - primer_tick) {  //limita los fps
			SDL_Delay(1000 / fps - (SDL_GetTicks() - primer_tick));
		}

		player->draw(screen);
		SDL_UpdateWindowSurface(window);
	}
	delete player;
	SDL_DestroyWindow(window);
	SDL_Quit();

	
	return 0;
}